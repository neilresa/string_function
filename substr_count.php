<?php
	$text = 'This is a test';
	echo strlen($text); 
	echo "<br>";

	echo substr_count($text, 'is');
	echo "<br>";
	echo substr_count($text, 'is', 3);
	echo "<br>";

	echo substr_count($text, 'is', 3, 3);
	echo "<br>";

	echo substr_count($text, 'is', 5, 10);
	echo "<br>";

	$text2 = 'gcdgcdgcd';
	echo substr_count($text2, 'gcdgcd');
?>