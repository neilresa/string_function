<?php

$number = 1234.56;


setlocale(LC_MONETARY, 'en_US');
echo money_format('%i', $number) . "\n";

setlocale(LC_MONETARY, 'it_IT');
echo money_format('%.2n', $number) . "\n";

$number = -1234.5672;


setlocale(LC_MONETARY, 'en_US');
echo money_format('%(#10n', $number) . "\n";

echo money_format('%=*(#10.2n', $number) . "\n";

setlocale(LC_MONETARY, 'de_DE');
echo money_format('%=*^-14#8.2i', 1234.56) . "\n";

setlocale(LC_MONETARY, 'en_GB');
$fmt = 'The final value is %i (after a 10%% discount)';
echo money_format($fmt, 1234.56) . "\n";


?>
